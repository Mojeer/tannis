open OUnit2

let dumps_dir = "./tests/dumps"

let rec skip_token nexttoken lexbuf =
  let token = nexttoken lexbuf in
  match token with
  | Parser.EOF -> ()
  | _ -> skip_token nexttoken lexbuf
;;

let lexer_test dumpfile test_ctxt =
  Filereader.parse_file skip_token dumpfile; ()
;;

let parser_test dumpfile test_ctxt =
  Filereader.parse_file Parser.objects dumpfile; ()
;;

let generate_tests label_prefix test_function dumpfile =
  let name = Filename.remove_extension (Filename.basename dumpfile) in
  (label_prefix ^ name) >:: (test_function dumpfile)
;;

let () =
  let lexer_tests = List.map (generate_tests "lexer:" lexer_test) (Filereader.dir_contents dumps_dir) in
  let parser_tests = List.map (generate_tests "parser:" parser_test) (Filereader.dir_contents dumps_dir) in
  let test_suite = "tests" >::: (lexer_tests @ parser_tests) in
  run_test_tt_main test_suite
;;
