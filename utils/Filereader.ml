open Lexer

let format_error lexbuf = 
  let startp = Lexing.lexeme_start_p lexbuf
  and endp = Lexing.lexeme_end_p lexbuf in
  Printf.sprintf " Line: %d Col: %d-%d syntax error. "  
    (startp.pos_lnum)
    (startp.pos_cnum - startp.pos_bol + 1)
    (endp.pos_cnum - endp.pos_bol + 1)
;;

let parse_file parser file = 
  let input_file = open_in file in
  let lexbuf = Lexing.from_channel input_file in
  try (
    let objs = parser nextoutertoken lexbuf in
    close_in (input_file);
    objs
  )
  with Failure(e) -> (
    let error = format_error lexbuf in
    raise (Failure (e ^ error))
  )
;;

let rec sort = function
  | [] -> []
  | x :: l -> insert x (sort l)

and insert elem = function
  | [] -> [elem]
  | x :: l ->
      if (String.lowercase_ascii elem) < (String.lowercase_ascii x) then elem :: x :: l else x :: insert elem l
;;

let dir_contents dir =
  let rec loop result = function
    | f::fs when Sys.is_directory f ->
          Sys.readdir f
          |> Array.to_list
          |> List.map (Filename.concat f)
          |> List.append fs
          |> sort
          |> loop result
    | f::fs -> loop (f::result) fs
    | []    -> result
  in
    loop [] [dir]
;;