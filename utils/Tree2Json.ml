open Tree

type json = 
 | Assoc of (string * json) list
 | Bool of bool
 | Float of float
 | Int of int
 | List of json list
 | Null
 | String of string 

let rec json_of_value (value: Tree.value) =
  match value with
  | Null -> `Null
  | Bool(b) -> `Bool(b)
  | Int(i) -> `Int(i)
  | Float(f) -> `Float(f)
  | String(s) -> `String(s)
  | Path(cn, p) -> `String(p) (* cname is ignored *)
  | Dict(d) -> `Assoc(List.map json_of_prop d)
  | Array(a) -> `List(List.map json_of_value a)

and json_of_prop (prop: Tree.prop) =
  (prop.name, json_of_value prop.value)
;;

let json_of_cprops (cprops: Tree.cprops) = (* cname is ignored *)
   List.map json_of_prop cprops.props
;;

let json_of_obj (obj: Tree.obj) = (* cname and path are ignored *)
  `Assoc(List.concat (List.map json_of_cprops obj.cprops))
;;