MENHIR := menhir
OCAMLBUILD := ocamlbuild
MENHIRFLAGS := --table --dump --explain
OCAMLFLAGS := -use-ocamlfind -use-menhir -menhir "$(MENHIR) $(MENHIRFLAGS)" -package menhirLib

EXT := native

MAIN_DIR := main
MAIN_FILES = $(shell find $(MAIN_DIR) -name '*.ml')
MAIN := $(basename $(notdir $(MAIN_FILES)))
MAIN_BINS := $(addsuffix .$(EXT), $(MAIN))

TOOLS_DIR := tools
TOOLS_FILES = $(shell find $(TOOLS_DIR) -name '*.ml')
TOOLS := $(basename $(notdir $(TOOLS_FILES)))
TOOLS_BINS := $(addsuffix .$(EXT), $(TOOLS))

TESTS_DIR := tests
TESTS_FILES = $(shell find $(TESTS_DIR) -name '*.ml')
TESTS := $(basename $(notdir $(TESTS_FILES)))
TESTS_BINS := $(addsuffix .$(EXT), $(TESTS))
RUN_TESTS := $(addprefix test-, $(TESTS))

MLY_FILES := $(shell find . -name '*.mly' )
MLL_FILES := $(shell find . -name '*.mll' )
ML_FILES := $(shell find . -name '*.ml' )

ERRORS_FILE := _build/errors.txt


# Builds
main: $(MAIN_BINS)
tools: $(TOOLS_BINS)

$(MAIN_BINS): $(ML_FILES) $(MLL_FILES) $(MLY_FILES) Makefile
	$(OCAMLBUILD) $(OCAMLFLAGS) $(MAIN_DIR)/$@

$(TOOLS_BINS): $(ML_FILES) $(MLL_FILES) $(MLY_FILES) Makefile
	$(OCAMLBUILD) $(OCAMLFLAGS) $(TOOLS_DIR)/$@

$(TESTS_BINS): $(ML_FILES) $(MLL_FILES) $(MLY_FILES) Makefile
	$(OCAMLBUILD) $(OCAMLFLAGS) $(TESTS_DIR)/$@

# Tests
test: $(RUN_TESTS)

$(RUN_TESTS): $(TESTS_BINS)
	./$(addsuffix .$(EXT), $(subst test-,,$@))

list-errors: main
	menhir --list-errors parser/*.mly --base result > $(ERRORS_FILE)

# Clean
clean:
	cat ./.tannis;
	rm -f $(ERRORS_FILE);
	$(OCAMLBUILD) -clean

.PHONY: main tools clean test list-errors
