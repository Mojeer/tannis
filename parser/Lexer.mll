{
  open Parser
}

let letter = ['a'-'z' 'A'-'Z']
let digit = ['0'-'9'] 
let float = digit+ '.' digit+
let ident = (letter | '_') (letter | digit | '_')*

let string = [^'"']*

let space = [' ' '\t']
let newline = ['\n' '\r'] | '\r' '\n'
let path = letter (digit | letter | '-' | '.' | ':' | '_')*

let notnewline = [^ '\n' '\r']
let outerassign = [^ '\n' '\r' '='] notnewline*

rule nextoutertoken = parse
  | space+                         { nextoutertoken lexbuf }
  | newline                        { Lexing.new_line lexbuf;  nextoutertoken lexbuf }
  | eof                            { EOF }
  | "*** Property dump for object" { LOBJHEADER }
  | "***"                          { ROBJHEADER }
  | "==="                          { LPROPSHEADER }
  | "properties ==="               { RPROPSHEADER }
  | "(" | "["                      { LPAR }
  | ")" | "]"                      { RPAR }
  | "'"                            { QUOTE }
  | digit+ as nb                   { INT (int_of_string nb) }
  | ident as str                   { IDENT str }
  | "=" ((outerassign? as v) newline as vn)  {
      Lexing.new_line lexbuf;
      let lexbuf = Lexing.from_string vn in
      try OUTERASSIGN (Parser.value_eol nextvaluetoken lexbuf)
      with e -> OUTERASSIGN (Tree.String v)      
    }
  | path as p                      { PATH p }

and nextvaluetoken = parse
  | space+            { nextvaluetoken lexbuf }
  | newline           { EOL }
  | "("               { LPAR }
  | ")"               { RPAR }
  | "["               { LBRA }
  | "]"               { RBRA }
  | "'"               { QUOTE }
  | "."               { DOT }
  | ":"               { COLON }
  | ","               { COMMA }
  | "="               { ASSIGN }
  | "-"               { DASH }
  | "True"            { TRUE } 
  | "False"           { FALSE }
  | "None"            { NONE }
  | "\"" (string as str) "\""  { STRING str }
  | '-'? digit+ as nb { INT (int_of_string nb) }
  | '-'? float as nb  { FLOAT (float_of_string nb) }
  | ident as str      { IDENT str }
  | path as p         { PATH p }