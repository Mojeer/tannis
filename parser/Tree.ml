type value =
  | Null
  | Bool of bool
  | Int of int
  | Float of float
  | String of string
  | Path of string * string
  | Dict of prop list
  | Array of value list

and prop = {
    name : string;
    value: value; 
}

type cprops = {
  cname: string;
  props: prop list;
}

type obj = {
  cname: string;
  path: string;
  cprops: cprops list
}
