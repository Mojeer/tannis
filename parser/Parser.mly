%{
  open Tree

  type temp_prop =
    | PropIndexed of int * prop
    | Prop of prop
  ;;

  let rec insert_in_array prop (arr: prop list) =
    match arr with
    | [] -> [{ name=prop.name; value=Tree.Array([prop.value]) }]
    | { name=n; value=Tree.Array(a) }::tl when n=prop.name -> { name=prop.name; value=Tree.Array(prop.value::a)}::tl
    | hd::tl -> hd::(insert_in_array prop tl)

  let rec resolve_temp_props values =
    match values with
    | [] -> []
    | Prop(p)::tl -> p::(resolve_temp_props tl)
    | PropIndexed(i, p)::tl -> insert_in_array p (resolve_temp_props tl)
  ;;
%}

%start objects
%type <Tree.obj list> objects

%start value_eol
%type <Tree.value> value_eol

%%

objects:
 | o=obj* EOF { o }

obj:
 | LOBJHEADER QUOTE cn=IDENT p=PATH QUOTE ROBJHEADER cp=cprops* { { cname=cn; path=p; cprops=cp } }

cprops:
 | LPROPSHEADER cn=IDENT RPROPSHEADER p=outerprop* { { cname=cn; props=(resolve_temp_props p) } }

outerprop:
 | p=IDENT v=OUTERASSIGN { Prop { name=p; value=v } }
 | p=IDENT LPAR i=INT RPAR v=OUTERASSIGN { PropIndexed (i, { name=p; value=v }) }

prop:
 | p=IDENT ASSIGN v=value { { name=p; value=v } }
 | p=IDENT ASSIGN { { name=p; value=Tree.Null } }

value_eol:
 | v=value EOL { v }

value:
 | NONE { Tree.Null }
 | TRUE { Tree.Bool(true) }
 | FALSE { Tree.Bool(false) }
 | i=INT { Tree.Int i }
 | f=FLOAT { Tree.Float f }
 | s=STRING { Tree.String s }
 | i=IDENT QUOTE p=PATH QUOTE { Tree.Path(i, p) }
 | i=IDENT { Tree.String i }
 | LPAR p=prop RPAR { Tree.Dict [p] }
 | LPAR p=prop COMMA l=separated_list(COMMA,prop) RPAR { Tree.Dict (p::l) }
 | LPAR v=value RPAR { Tree.Array [v] }
 | LPAR v=value COMMA l=separated_list(COMMA,value) RPAR { Tree.Array (v::l) }