%{
%}

/**********/
/* Tokens */
/**********/

%token ASSIGN

/* Literal values */
%token <int> INT
%token <float> FLOAT
%token TRUE FALSE
%token NONE
%token <string> STRING
%token <string> PATH

/* Identifiers */
%token <string> IDENT

/* Separators */
%token EOF EOL QUOTE
%token LPAR RPAR
%token LBRA RBRA
%token DOT COMMA COLON DASH

/* Outer parser */
%token LPROPSHEADER RPROPSHEADER
%token LOBJHEADER ROBJHEADER
%token <Tree.value> OUTERASSIGN

%%