let print_obj (obj: Tree.obj) =
  print_endline ("=== " ^ obj.path ^ "===");
  print_endline (Yojson.Basic.pretty_to_string (Tree2Json.json_of_obj obj))

let parse_objs file =
  let objs = Filereader.parse_file Parser.objects file in
  List.iter print_obj objs
      
let _ = Arg.parse [] parse_objs ""
