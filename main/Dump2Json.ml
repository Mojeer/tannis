let save2json path (obj: Tree.obj) =
  print_endline ("    " ^ obj.path);
  let dir_path = (path ^ obj.cname) in
  if not (Sys.file_exists dir_path) then Unix.mkdir dir_path 0o755;
  let outfile = (dir_path ^ "/" ^ obj.path ^ ".json") in
  let outfile = open_out outfile in
  Yojson.Basic.pretty_to_channel outfile (Tree2Json.json_of_obj obj);
  close_out (outfile)
;;

let parse_objs json_path file =
  print_endline ("Parsing " ^ file);
  let objs = Filereader.parse_file Parser.objects file in
  List.iter (save2json json_path) objs
;;

let parse_in_dir dumps_dir =
  let json_path = "./jsons/" in
  if not (Sys.file_exists json_path) then Unix.mkdir json_path 0o755; (* Won't work on windows *)
  let files = (Filereader.dir_contents dumps_dir) in
  List.iter (parse_objs json_path) files
;;
      
let _ = Arg.parse [] parse_in_dir ""
