open Printf

let rec iter_with_indent indent funct l =
  match l with
  | [] -> ()
  | hd::[] -> funct (indent ^ "   ") (indent ^ "└─ ") hd
  | hd::tl -> funct (indent ^ "│  ") (indent ^ "├─ ") hd; iter_with_indent indent funct tl
;;

let rec print_indented_value indent indent_current (value: Tree.value) =
  print_string indent_current;
  print_value indent value
and print_value indent (value: Tree.value) =
  match value with
  | Null -> printf "None\n"
  | Bool(b) -> printf "%B\n" b
  | Int(i) -> printf "%i\n" i
  | Float(f) -> printf "%f\n" f
  | String(s) -> printf "\"%s\"\n" s
  | Path(c, p) -> printf "%s'%s'\n" c p
  | Dict(d) -> printf "Dict()\n"; iter_with_indent indent print_prop d
  | Array(a) -> (
      print_string "Array[]\n";
      iter_with_indent indent print_indented_value a
  )
and print_prop indent indent_current (prop: Tree.prop) =
  printf "%sProperty(%s)=" indent_current prop.name;
  print_value indent prop.value
;; 

let print_cprops indent indent_current (cprops: Tree.cprops) =
  printf "%sClassProperties(%s)\n" indent_current cprops.cname;
  iter_with_indent indent print_prop cprops.props
;;

let print_obj indent (obj: Tree.obj) =
  printf "%sObject(%s %s)\n" indent obj.cname obj.path;
  iter_with_indent indent print_cprops obj.cprops
;;

let parse_objs file =
  let objs = Filereader.parse_file Parser.objects file in
  List.iter (print_obj "") objs
;;
      
let _ = Arg.parse [] parse_objs ""
