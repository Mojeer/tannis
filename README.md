# tannis
***tannis*** is an ocaml library for parsing Borderlands dumps.
It also comes with an utility to convert them as json

## Usage

### As an utility
```bash
./Dump2json.native path/to/directory/with/dumps/
```
will convert .dump to .jsons

```bash
./Print2Json.native path/to/directory/with/dumps/
```
will print the json representation of a .dump

### As an ocaml library
Not documented.
But take a look at the files in tools/ and main/ for examples 

## Supported games
- [ ] Borderlands 3 (Hopefully bl3 modding will be as proficient as it is for the others)
- [x] Borderlands 2
- [ ] Borderlands The Pre Sequel (Probably already supported, I haven't tested it yet)
- [ ] Borderlands
- [ ] Maybe other UE games, let me know if you tried

## Build

### Dependencies
- [menhir](https://opam.ocaml.org/packages/menhir/)
- [ounit](https://opam.ocaml.org/packages/ounit/)
- [yojson](https://opam.ocaml.org/packages/yojson/)

### Make commands
- make / make main: build executables
- make tools: build debug tools
- make test: run all tests
- make list-errors: create _build/errors.txt wich list errors reported by menhir
- make clean: clean, with style

### Dev tools
- PrintTree path_to_dumpfile: print the parsed tree

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MPL](https://www.mozilla.org/en-US/MPL/2.0/FAQ/)
